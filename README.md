# pre-commit template for Python

## Introduction
Git hook scripts are useful for identifying simple issues before submission to code review. We run our hooks on every commit to automatically point out issues in code such as missing semicolons, trailing whitespace, and debug statements. By pointing these issues out before code review, this allows a code reviewer to focus on the architecture of a change while not wasting time with trivial style nitpicks.

Comparing with adding these checks directly into CI/CD pipeline, it's significantly faster to check our commits before pushing the code to remote host to see what needs to be fixed.
[Supported Hooks](/https://pre-commit.com/hooks.html/)

## Installation
```
# install pre-commit
pip install pre-commit
pre-commit install
```

## Config
If you want to start with a simple default config by using the following code block:

```
# Simple config
pre-commit sample-config > .pre-commit-config.yaml cat .
pre-commit-config.yaml
```

The following config is the current standard config that works with our CI/CD pipeline. This config will:
1. enforce black formator
2. run flake8 for static analysis (enforcing PEP8)
3. auto update versions
4. sort your imports
5. run other standard checks, such as protecting your master/main branch

## Run
Though pre-commit hooks are meant to run before (pre) a commit, we can manually trigger all or individual hooks on all or a set of files.

```
# Run pre-commit run --all-files # run all hooks on all files
pre-commit run <HOOK_ID> --all-files # run one hook on all files
pre-commit run --files <PATH_TO_FILE> # run all hooks on a file
pre-commit run <HOOK_ID> --files <PATH_TO_FILE> # run one hook on a file
```

## Skip
It is highly not recommended to skip running any of the pre-commit hooks because they are there for a reason. But for some highly urgent, world saving commits, we can use the no-verify flag.

```
# Commit without hooks
git commit -m <MESSAGE> --no-verify
```


## Known issues and fixes
1. ssl certificate fail
fix: $git config --global http.sslverify false
or $export GIT_SSL_NO_VERIFY=1

2. pip user error, pip install default with a user flag, does not work in venv
fix: $export PIP_USER=false
